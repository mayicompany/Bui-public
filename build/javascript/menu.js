/*菜单控件
 * @Author: kevin.huang 
 * @Date: 2018-07-21 12:28:05 
 * @Last Modified by: kevin.huang
 * @Last Modified time: 2019-04-04 08:44:46
 * Copyright (c): kevin.huang Released under MIT License
 */
(function (global, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['$B'], function (_$B) {
            return factory(global, _$B);
        });
    } else {
        if (!global["$B"]) {
            global["$B"] = {};
        }
        factory(global, global["$B"]);
    }
}(typeof window !== "undefined" ? window : this, function (window, $B) {
    var defOpts = {
        textField: 'text',
        ellipsis:false,
        idField: 'id',
        scrollStyle:{
            size: '8px',
            display:'auto',
            setPadding:false,
            hightColor:"#1590FA",
            slider: {
                "background-color": '#6CB7FA',
                "border-radius": "8px",
            },bar:{
                "background-color": "#E8E8E8",
                "border-radius": "8px",
                "opacity": 0.5
            }   
        }
    };
    /***
   *opts = {
       data: null, //'数据'       
       textField: 'text', //菜单名称字段，默认为text
       idField: 'id', //菜单id字段,默认为id
       onClick:fn(data)    		
   }
   ****/
    function Menu(jqObj, opts) {
        $B.extend(this, Menu); //继承父类        
        this.jqObj = jqObj.addClass("k_box_size k_menu_wrap").css({       
            "height": "100%",
            "width": "100%",
            "overflow": "visible"
        });
        this.scrollWrap = $("<div style='height:100%;width:100%;padding-right:0px;padding-bottom:2px;' class='k_menu_wrap_scroll k_box_size'></div>").appendTo(this.jqObj);
        this.treeWrap = $("<div class='k_menu_wrap_ul clearfix' ></div>").appendTo(this.scrollWrap);      
        this.opts = $.extend(true,{} , defOpts, opts);
        var datas = this.opts.data;
        var vDom = $("<div/>");
        this.firstItem = undefined;
        this._createList(vDom, datas, 1);
        this.maxWidth = 0;
        vDom.children().detach().appendTo(this.treeWrap);        
        if (this.firstItem) {
            this.firstItem.trigger("click");
        }
        this.scrollObj = this.scrollWrap.myscrollbar(this.opts.scrollStyle).getMyScrollIns();
        this._setWrapWidth();
        this._setMinWidth();
        var _this = this;
        $(window).resize(function(){
            _this._setMinWidth();
        });
    }
    Menu.prototype = {
        _setMinWidth:function(){
            var minWidth = this.treeWrap.parent().width();            
            if(this.opts.ellipsis){
                this.rootUl.css("width",minWidth);
            }else{
                this.rootUl.css("min-width",minWidth);
            }
        },
        _itClickEnvent: function (e) {
            var _this = e.data._this;
            var $a = $(this);
            var mdata = $a.data("mdata");
            var isParent = typeof mdata.children !== 'undefined';
            if (isParent) {
                var $ul = $a.next("ul").slideToggle("fast", function () {
                    var $icon = $a.children(".k_menu_parent_icon");
                    if ($ul.css("display") === "none") {
                        $icon.removeClass("fa-angle-down").addClass("fa-angle-left").css("padding-right","6px");
                    } else {
                        $icon.removeClass("fa-angle-left").addClass("fa-angle-down").css("padding-right","3px");
                    }
                    _this._setWrapWidth();
                    _this.scrollObj.resetSliderPosByTimer();
                });
            }
            if (_this.opts.onClick) {
                setTimeout(function () {
                    _this.opts.onClick.call($a, mdata.data ? mdata.data : mdata, isParent);
                }, 10);
            }
            if (isParent) {
                return;
            }
            if (_this.activedItem) {
                _this.activedItem.removeClass("k_menu_actived");
            }
            _this.activedItem = $a.addClass("k_menu_actived");
        },
        _setWrapWidth:function(){           
            if(this.opts.ellipsis){
                this.treeWrap.width("100%");
            }else{
                this.treeWrap.width(this.rootUl.width());
            }
        },
        _createList: function (wrap, datas, deep) {
            var ul = $("<ul class='k_menu_ul' style='width:auto;'/>").appendTo(wrap);
            if(!this.opts.ellipsis){//,"float":"left"
                ul.css({"min-width" :"100%"});
            }
            if (deep === 1) {
               this.rootUl = ul.addClass("k_menu_ul_root");
            }
            for (var i = 0, len = datas.length; i < len; ++i) {
                var data = datas[i];
                var txt = data[this.opts.textField];
                var li = $("<li style='white-space: nowrap;width:auto;list-style:none;' ><a style='display:inline-block;white-space:nowrap;padding-right:10px;' class='k_box_size'>" + txt + "</a></li>").appendTo(ul);
                var $a = li.children("a");
                if(this.opts.ellipsis){
                    $a.css({"width":"100%" , "max-width" : "100%","text-overflow" :" ellipsis","overflow":"hidden","padding-right":"0px"});
                }else{
                    $a.css("min-width","100%");
                }
                $a.attr("title",txt);
                var icon = data.data.menuIconCss;               
                var r = 15;
                if (deep === 1) {
                    r = 12;
                }
                $a.css("padding-left", deep * r);
                if (icon && icon !== "") {
                    $a.prepend("<i style='padding-right:6px;' class='fa " + icon + "'><i>");
                }                
                if (data.children) {
                    $a.prepend("<i class='fa fa-angle-down k_menu_parent_icon'></i>");
                    // console.log("is parent >");
                    // if(this.opts.ellipsis){                       
                    // }else{
                    //     $a.prepend("<i class='fa fa-angle-down k_menu_parent_icon'></i>");
                    // }                    
                    this._createList(li, data.children, deep + 1);
                } else {
                    if (!this.firstItem) {
                        this.firstItem = $a;
                    }
                }
                $a.on("click", { _this: this }, this._itClickEnvent).data("mdata", data);
                if(this.opts.onItemCreated){
                    this.opts.onItemCreated.call(li,data, typeof data.children !== 'undefined');
                }
            }
        },
        _loopParent: function (txtArray, aEle) {
            var ul = aEle.parent().parent();
            if (!ul.hasClass("k_menu_ul_root")) {
                var ele = ul.prev("a");
                var txt = ele.data("mdata").text;
                txtArray.push(txt);
                this._loopParent(txtArray, ele);
            }
        },
        getTreeTxtPath: function () {
            var item = this.activedItem;
            var arr = [];
            var txt = item.data("mdata").text;
            arr.push(txt);
            this._loopParent(arr, item);
            return arr.reverse();
        },
        destroy:function(){          
            this.scrollObj.destroy();
            this.super.destroy.call(this);
        }
    };
    $B["Menu"] = Menu;
    return Menu;
}));