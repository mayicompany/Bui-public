/*
 * @Author: kevin.huang 
 * @Date: 2018-07-21 19:48:42 
 * @Last Modified by: kevin.huang
 * @Last Modified time: 2019-04-09 22:37:20
 * Copyright (c): kevin.huang Released under MIT License
 */
(function (global, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['$B'], function (_$B) {
           return factory(global, _$B);
        });
    } else {
        if(!global["$B"]){
            global["$B"] = {};
        }
        factory(global, global["$B"]);
    }
}(typeof window !== "undefined" ? window : this, function (window, $B) {//
    var iframeHtml = "<iframe  class='panel_content_ifr' frameborder='0' style='overflow:visible;display:block;vertical-align:top;' scroll='none'  width='100%' height='100%' src='' ></iframe>";
    var loadingHtml = "<div class='k_box_size' style='position:absolute;z-index:2147483600;width:100%;height:26px;top:2px;left:0;' class='loading'><div class='k_box_size' style='filter: alpha(opacity=50);-moz-opacity: 0.5;-khtml-opacity: 0.5;opacity: 0.5;position:absolute;top:0;left:0;width:100%;height:100%;z-index:2147483600;background:#03369B;'></div><div class='k_box_size' style='width:100%;height:100%;line-height:26px;padding-left:16px;position:absolute;width:100%;height:100%;z-index:2147483611;color:#fff;text-align:center;'><i style='color:#fff;font-size:16px;' class='fa animate-spin fa-spin6'></i><span style='padding-left:5px;font-weight:bold;color:#fff;'>" + $B.config.loading + "</span></div></div>";
 
    /**加载数据**/
    function _load(onLoaded) {
        var _this = this;
        var opt = this.data("data");
        var isFun = typeof onLoaded === 'function';
        _this.children().remove();
        var loading;
        var scrollWrap = this;
        loading = $(loadingHtml).appendTo(_this);
        if (opt.dataType === "html") {         
            $B.htmlLoad({
                target: _this,
                url: opt.url,
                onLoaded: function () {
                    loading.fadeOut(function(){
                        $(this).remove();
                    });
                    if (isFun) {
                        onLoaded.call(_this, opt.title);
                    }
                    $B.bindTextClear(_this);
                }
            });
        } else if (opt.dataType === "json") {           
            $B.request({
                dataType: 'json',
                url: opt.url,
                ok: function (message, data) {
                    if (isFun) {
                        onLoaded.call(_this, opt.title, data);
                    }
                },
                final: function (res) {
                    loading.fadeOut(function(){
                        $(this).remove();
                    });
                }
            });
        } else {            
            var iframe = $(iframeHtml);
            var ifr = iframe[0];
            iframe.on("load",function(){
                loading.fadeOut(function(){
                    $(this).remove();
                });
                if (isFun) {
                    onLoaded.call(_this, opt.title);
                }
            });
            
            ifr.src = opt.url;
            iframe.appendTo(_this);
        }
        //绑定scroll
        // if (!scrollWrap.data("isscroll")) {
        //     $B.scrollbar(scrollWrap);
        //     scrollWrap.data("isscroll", true);
        // }
    }

    function Labeltab(jqObj, opts) {
        $B.extend(this, Labeltab);
        this.jqObj = jqObj.addClass("k_labeltab_main");
        this.opts = opts;
        this.wrap = $("<div class='k_labeltab_wrap k_box_size'></div>").appendTo(this.jqObj);
        this.title = $("<div class='k_labeltab_title k_box_size'></div>").appendTo(this.wrap);
        var _h = this.title.outerHeight();
        this.body = $("<div class='k_labeltab_body k_box_size'></div>").appendTo(this.wrap).css("border-top", _h + "px solid #fff");
        var defaultIt = null,
            limit = opts.tabs.length - 1,
            _this = this;
        this.activedItem = null;
        var _click = function () {
            var $this = $(this);
            var left = (($this.width() - 11) / 2) + $this.position().left + 5;
            var idx = parseInt($this.attr("i"));
            var it = _this.body.children(".k_labeltab_body_item").eq(idx);
            if (_this.activedItem !== null) {
                _this.activedItem.hide();
            }
            _this.activedItem = it.show();
            _this.$actived.animate({
                left: left
            }, 220, function () {
                if (_this.activedItem.children().length === 0) {
                    if (typeof _this.activedItem.data("data").dataType !== 'undefined') {
                        _load.call(_this.activedItem, _this.opts.onLoaded);
                    }
                }
            });
            if (opts.onclick) {
                var title = $this.attr("_title");
                setTimeout(function () {
                    opts.onclick.call($this,title);
                },10);
            }
        };
        for (var i = 0, len = opts.tabs.length; i < len; i++) {
            var opt = opts.tabs[i];
            var title = opt.title;
            var html = "<a i='" + i + "' _title='" + title + "'>" + title + "</a>";
            var $it = $(html).appendTo(this.title).click(_click);

            if (i < limit) {
                this.title.append("|");
            }
            var itBody = $("<div class='k_labeltab_body_item'></div>").appendTo(this.body);
            if (opt.actived || i === 0) {
                defaultIt = $it;
            }
            if (opt.url && opt.url !== "") {
                itBody.data("data", {
                    url: opt.url,
                    dataType: opt.dataType,
                    title: opt.title
                });
            } else {
                $B.scrollbar(itBody);
                itBody.append(opt.content);
            }
            if (opt.iconCls && opt.iconCls !== '') {
                $it.addClass("btn").prepend("<i class='fa " + opt.iconCls + "'></i>&nbsp");
            }
        }
        this.$actived = $('<div style="height: 6px; left:0px;" class="actived"></div>').appendTo(this.title);
        if (defaultIt !== null) {
            defaultIt.trigger('click');
        }
    }
    $B["Labeltab"] = Labeltab;
    return Labeltab;
}));