/*
 * @Author: kevin.huang 
 * @Date: 2018-07-21 19:48:42 
 * @Last Modified by: kevin.huang
 * @Last Modified time: 2019-04-09 22:34:01
 * Copyright (c): kevin.huang Released under MIT License
 */
(function (global, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['$B', 'plugin', "panel", "ctxmenu"], function (_$B) {
            return factory(global, _$B);
        });
    } else {
        if(!global["$B"]){
            global["$B"] = {};
        }
        factory(global, global["$B"]);
    }
}(typeof window !== "undefined" ? window : this, function (window, $B) {// _this.iframe.css({"vertical-align":"top","display":"block"});
    var hasIns = false;   
    window["_tab_more_close_fn"] = function(){
        if(hasIns){
            var moreWraps = $("body").children(".k_tab_more_menu_wrap");      
            moreWraps.hide();
        }        
    }; 
    /***全局关闭***/
    $(window).on('mouseup.tab_more_close', function () { 
        if(hasIns){
            window["_tab_more_close_fn"]();
        }        
        setTimeout(function() {            
            if(window.parent !== window.self && window.parent["_tab_more_close_fn"]){              
                window.parent["_tab_more_close_fn"]();
            }
        }, 10);
    });
    var iframeHtml = "<iframe  class='panel_content_ifr' frameborder='0' style='overflow:visible;padding:0;margin:0;display:block;vertical-align:top;' scroll='none'  width='100%' height='100%' src='' ></iframe>";
    var loadingHtml = "<div class='k_box_size' style='position:absolute;z-index:2147483600;width:100%;height:26px;top:2px;left:0;' class='loading'><div class='k_box_size' style='filter: alpha(opacity=50);-moz-opacity: 0.5;-khtml-opacity: 0.5;opacity: 0.5;position:absolute;top:0;left:0;width:100%;height:100%;z-index:2147483600;background:#03369B;'></div><div class='k_box_size' style='width:100%;height:100%;line-height:26px;padding-left:16px;position:absolute;width:100%;height:100%;z-index:2147483611;color:#fff;text-align:center;'><i style='color:#fff;font-size:16px;' class='fa animate-spin fa-spin6'></i><span style='padding-left:5px;font-weight:bold;color:#fff;'>" + $B.config.loading + "</span></div></div>";
    /**加载数据**/
    function _load(onLoaded) {        
        var _this = this;
        var opt = this.data("data");
        if(!opt){
            return;
        }
        var isFun = typeof onLoaded === 'function';
        _this.children().remove();
        _this.html("");
        var loading;
        var url = opt.url;
        if(url.indexOf("?") > 0){
            url = url + "&_t_="+$B.generateMixed(5);
        }else{
            url = url + "?_t_="+$B.generateMixed(5);
        }
        loading = $(loadingHtml).appendTo(_this);
        if (opt.dataType === "html") {           
            $B.htmlLoad({
                target: _this,
                url: url,
                onLoaded: function () {
                    loading.fadeOut(function(){
                        $(this).remove();
                    });
                    if (isFun) {
                        onLoaded.call(_this, opt.title);
                    }
                    $B.bindTextClear(_this);
                }
            });
        } else if (opt.dataType === "json") {           
            $B.request({
                dataType: 'json',
                url: url,
                ok: function (message, data) {
                    if (isFun) {
                        onLoaded.call(_this, opt.title, data);
                    }
                },
                final: function (res) {
                    loading.fadeOut(function(){
                        $(this).remove();
                    });
                }
            });
        } else {
            var iframe = $(iframeHtml);
            var ifr = iframe[0];         
            iframe.on("load",function(){
                loading.fadeOut(function(){
                    $(this).remove();
                });               
                if (isFun) {
                    onLoaded.call(_this, opt.title);
                }
            });
            ifr.src = url;            
            iframe.appendTo(_this);
        }
    }
    /**创建**/
    function _createItem(_opt, lineHeight) {
        var _this = this;
        var clrAttr = "line-height:" + lineHeight + "px;";
        var isClick = typeof _this.opts.onClick === 'function';
        var isCloseFn = typeof _this.opts.onClosed === 'function';
        var $li = $("<li  title='" + _opt.title + "' class='k_box_size' style='" + clrAttr + "'>" + _opt.title + "</li>").appendTo(this.$ul).click(function (e) {
            _this.$moreWrap.hide();
            var $t = $(this),
                index = 0,
                prev = $t.prev();
            while (prev.length > 0) {
                index++;
                prev = prev.prev();
            }
            var currentActived = _this.$ul.data("actived");
            if (currentActived && currentActived.attr("title") !== $t.attr("title")) {
                _this.$ul.data("activedItem").css("z-index",-1);
                var color = _this.$ul.parent().parent().css("background-color");
                currentActived.animate({
                    "backgroundColor": color
                },100, function () {
                    currentActived.removeClass("actived");
                    currentActived.attr("style", clrAttr);
                    if (isClick && !e.isTrigger) {
                        var data = $t.data("itdata");
                        _this.opts.onClick.call($t, $t.attr("title"),data);
                    }
                });
            }
            _this.$ul.data("actived", $t.addClass("actived"));
            $t.attr("style", clrAttr);
            var currentIt = _this.$body.children(".k_tab_item").eq(index).css("z-index",1);
            _this.$ul.data("activedItem", currentIt);
            currentIt.siblings().css("z-index",-1);
            if (_this.ctxmenu) {
                _this.ctxmenu.hide();
            }
            return false;
        });
        if(_opt.data){
            $li.data("itdata", _opt.data);
        }
        if (_opt.iconCls && _opt.iconCls !== '') {
            $li.prepend('<i class="fa ' + _opt.iconCls + '"></i>&nbsp');
        }
        var $it = $("<div style='z-index:-1;overflow: auto;background:#ffffff;' class='k_tab_item k_box_size'></div>").appendTo(this.$body);
        if (_opt.url && _opt.url !== "") {
            $it.data("data", {
                dataType: _opt.dataType,
                url: _opt.url,
                onLoaded: _opt.onLoaded,
                title: _opt.title
            });
            _load.call($it, _this.opts.onLoaded);
        } else if(_opt.content){
            $it.html(_opt.content);
            //$B.scrollbar($it);
        }
        if (_opt.actived) {
            $li.trigger("click");
        }
        if (_opt.closeable) {
            $("<a style='display:none;' class='close'><i class='small-font smallsize-font fa fa-cancel-2 '></i></a>").appendTo($li).click(function () {
                var $p = $(this).parent();
                var $t;
                _this.$moreWrap.hide();
                if ($p.hasClass("actived")) {
                    var next = $p.next();
                    if (next.length > 0) {
                        $t = next.trigger("click");
                    } else {
                        var prev = $p.prev();
                        if (prev.length > 0) {
                            $t = prev.trigger("click");
                        } else {
                            _this.removeData("activedItem");
                            _this.removeData("actived");
                        }
                    }
                }
                var tmp = $p.prev(),
                    idx = 0;
                while (tmp.length > 0) {
                    tmp = tmp.prev();
                    idx++;
                }
                var _tilte = $p.attr("title");
                var itData = $p.data("itdata");
                $p.remove();
                _this.$body.children().eq(idx).remove();
                var ul_left = _this.$ul.position().left;
                var lsIt = _this.$ul.children().last();
                var lsdiff = lsIt.position().left + lsIt.outerWidth() + ul_left;
                if (ul_left < 0) {
                    if (lsdiff <= _this.$headerUlWrap.width()) {
                        _this.rightButton.addClass("k_tab_icon_btn_disabeld");
                    }
                    var shiftDiff = _this.$headerUlWrap.width() - lsdiff;
                    var newLeft = ul_left + shiftDiff;
                    if (newLeft > 0) {
                        newLeft = 0;
                        _this.leftButton.removeClass("k_tab_icon_btn_disabeld");
                    }
                    _this.$ul.animate({
                        "left": newLeft
                    }, 150);
                } else {
                    _this.leftButton.addClass("k_tab_icon_btn_disabeld");
                    if (lsdiff <= _this.$headerUlWrap.width()) {
                        _this.rightButton.removeClass("k_tab_icon_btn_disabeld");
                    }
                }
                if (isCloseFn) {
                    setTimeout(function () {
                        _this.opts.onClosed(_tilte,itData);
                    }, 1);
                }
            });
            $li.on({
                mouseenter:function(){
                    var $t = $(this);
                    $t.children("a.close").show();
                },
                mouseleave:function(){
                    var $t = $(this);
                    $t.children("a.close").hide();
                }
            });
        }
        var liwidth = $li.outerWidth();
        var curLeft = liwidth + $li.position().left; //$B.Math.add(liwidth, $li.position().left);
        var wrapw = this.$headerUlWrap.outerWidth();
        var diff = curLeft - wrapw; //$B.Math.sub(curLeft, wrapw);
        if (diff > 0) {
            this.leftButton.show().removeClass("k_tab_icon_btn_disabeld");
            this.rightButton.show().addClass("k_tab_icon_btn_disabeld");
            var _tmp = diff.toString();
            if (_tmp.indexOf(".") > 0) {
                diff = parseInt(_tmp.split(".")[0]) + 2;
            }
            this.$ul.animate({
                "left": -diff
            }, 150);
        }
        return $li;
    }
    function Tab(jqObj, opts) {       
        hasIns = true;
        $B.extend(this, Tab);
        this.jqObj = jqObj.addClass("k_tab_main k_box_size");
        this.opts = opts;
        this.$header = $("<div class='k_tab_header_wrap k_box_size k_tab_icon k_tab_icon_header_bg'><div class='k_tab_header_wrap_bottom_border k_box_size'></div></div>").appendTo(this.jqObj).children();
        this.$headerUlWrap = $("<div class='k_tab_header_ulwrap'></div>").appendTo(this.$header);
        var _h = this.$header.outerHeight();
        this.$body = $("<div class='k_tab_item_body k_box_size'></div>").appendTo(this.jqObj).css("border-top", _h + "px solid #ffffff");
        this.leftButton = $("<div style='left:0' class='k_tab_button_wrap_wrap k_tab_left_btn k_box_size'><div><i class='fa fa-angle-double-left'></i></div></div>").appendTo(this.$header).children();
        this.rightButton = $("<div  style='right:0' class='k_tab_button_wrap_wrap k_tab_right_btn k_box_size'><div><i style='' class='fa fa-angle-double-right'></i></div></div>").appendTo(this.$header).children();
        this.$ul = $("<ul class='k_box_size'></ul>").appendTo(this.$headerUlWrap);
        var $b = $(window.document.body);
        this.$moreWrap = $("<div style='width:auto;height:auto;' class='k_tab_more_menu_wrap k_box_shadow'><ul></ul></div>").appendTo($b).hide();
        var lineHeight = this.$ul.height(),
            w = 0;
        for (var i = 0, l = this.opts.tabs.length; i < l; ++i) {
            w = w + _createItem.call(this, this.opts.tabs[i], lineHeight);
        }        
        var _this = this;
        this.leftButton.children("i").css("line-height",lineHeight+"px");
        this.rightButton.children("i").css("line-height",lineHeight+"px");
        function findRightLi() {
            var _lft = _this.$ul.position().left;
            var $end = _this.$ul.children().last();
            var endx = $end.outerWidth() + $end.position().left;
            var len = endx + _lft; 
            var wraplen = _this.$headerUlWrap.outerWidth();
            while (len > wraplen) {
                $end = $end.prev();
                endx = $end.outerWidth() + $end.position().left; 
                len = endx + _lft; 
            }
            var findLi = $end.next();
            if (findLi.length > 0) {
                return findLi;
            }
            return null;
        }
        this.rightButton.on({
            mouseenter: function () {
                var _$t = $(this);
                if (_$t.hasClass("k_tab_icon_btn_disabeld")) {
                    return;
                }
                var findLi = findRightLi();
                if (findLi !== null) {
                    var $ul = _this.$moreWrap.children();
                    $ul.children().remove();
                    var offset = _$t.offset();
                    _this.$moreWrap.css({
                        top: offset.top + _$t.outerHeight()
                    }).hide();
                    var help = 0;
                    var _click = function () {
                        var _$t = $(this);
                        var idx = parseInt(_$t.attr("idx"));
                        var count = _$t.parent().children().length;
                        var loop = count - idx;
                        var closeLi = _this.$ul.children().last();
                        if (loop === 1) {
                            _this.rightButton.addClass("k_tab_icon_btn_disabeld");
                        }
                        while (loop > 1) {
                            closeLi = closeLi.prev();
                            loop--;
                        }
                        closeLi.trigger("click");
                        var ulWrapLen = _this.$headerUlWrap.width();
                        var ul_left = Math.abs(_this.$ul.position().left);
                        var shift = closeLi.position().left + closeLi.outerWidth() - (ul_left + ulWrapLen);
                        if (shift > 0) {
                            _this.$ul.animate({
                                "left": -(ul_left + shift)
                            },200, function () {
                                if (typeof _this.opts.onClick === 'function') {
                                    _this.opts.onClick.call(closeLi, closeLi.attr("title"));
                                }
                            });
                            _this.leftButton.show().removeClass("k_tab_icon_btn_disabeld");
                        }
                        _this.$moreWrap.hide();
                        return false;
                    };
                    var _close = function () {
                        var _$t = $(this).parent();
                        var idx = parseInt(_$t.attr("idx"));
                        var count = _$t.parent().children().length;
                        var loop = count - idx;
                        var closeLi = _this.$ul.children().last();
                        while (loop > 1) {
                            closeLi = closeLi.prev();
                            loop--;
                        }
                        closeLi.find(".close").trigger("click");
                        var nextLi = _$t.next();
                        while (nextLi.length > 0) {
                            nextLi.attr('idx', idx--);
                            nextLi = nextLi.next();
                        }
                        var ul = _$t.parent();
                        _$t.remove();
                        if(ul.children().length === 0){
                            ul.parent().hide();
                        }
                        return false;
                    };
                    findLi.clone().appendTo($ul).attr("idx", help).click(_click).children(".close").click(_close);
                    var next = findLi.next();
                    while (next.length > 0) {
                        help++;
                        next.clone().appendTo($ul).attr("idx", help).click(_click).children(".close").click(_close);
                        next = next.next();
                    }
                    _this.$moreWrap.css("left", offset.left - _this.$moreWrap.outerWidth() + 16).show();
                } else {
                    _this.$moreWrap.hide();
                }
            },
            click: function () {
                _this.$moreWrap.hide();
                var _$t = $(this);
                var $icon = _$t.children();
                if ($icon.hasClass("k_tab_icon_right_disabeld")) {
                    return;
                }
                var findLi = findRightLi();
                if (findLi !== null) {
                    _this.leftButton.show().removeClass("k_tab_icon_btn_disabeld");
                    var _left = _this.$ul.position().left;
                    var showWidth = _this.$headerUlWrap.outerWidth() - (findLi.position().left + _left); 
                    var shift = findLi.outerWidth() - showWidth; 
                    var _tmp = (_left - shift).toString();
                    if (_tmp.indexOf(".") > 0) {
                         _tmp = parseInt(_tmp.split(".")[0]) - 2;
                    }
                    _this.$ul.animate({
                        "left": _tmp
                    }, 300, function () {
                        if (findLi.next().length === 0) {
                            _this.rightButton.removeClass("k_tab_icon_btn_disabeld");
                        }
                        _$t.trigger("mouseenter");
                    });
                } else {
                    _this.rightButton.addClass("k_tab_icon_btn_disabeld");
                }
                return false;
            }
        });
        this.leftButton.on({
            mouseenter: function () {
                var _$t = $(this);
                if (_$t.hasClass("k_tab_icon_btn_disabeld")) {
                    return;
                }
                var lft = _this.$ul.position().left;
                if (lft >= 0) {
                    _$t.addClass("k_tab_icon_btn_disabeld");
                    return;
                }
                var $start = _this.$ul.children().first();
                var $ul = _this.$moreWrap.children();
                $ul.children().remove();
                var offset = _$t.offset();
                _this.$moreWrap.css({
                    top: offset.top + _$t.outerHeight()
                }).hide();
                var help = 0;
                var absLft = parseInt(Math.abs(lft));
                var start_w = $start.outerWidth();
                var _pos = $start.position().left + start_w; 
                var diff = _pos - absLft; 
                var _click = function () {
                    var $li = $(this);
                    var idx = parseInt($li.attr("idx"));
                    var targetLi = _this.$ul.children().eq(idx).trigger("click");
                    var targetLeft = targetLi.position().left;
                    _this.$ul.animate({
                        "left": -targetLeft
                    }, 200, function () {
                        if (typeof _this.opts.onClick === 'function') {
                            _this.opts.onClick.call(targetLi, targetLi.attr("title"));
                        }
                    });
                    _this.$moreWrap.hide();
                    if (idx === 0) {
                        _$t.addClass("k_tab_icon_btn_disabeld");
                    }
                    if (_this.$ul.children().last().position().left > _this.$headerUlWrap.width()) {
                        _this.rightButton.removeClass("k_tab_icon_btn_disabeld");
                    }
                    return false;
                };
                var _close = function () {
                    var $li = $(this).parent();
                    var ul = $li.parent();
                    var idx = parseInt($li.attr("idx"));
                    var cli = _this.$ul.children().eq(idx);
                    cli.children(".close").trigger("click");
                    $li.remove();
                    if(ul.children().length === 0){
                        ul.parent().hide();
                    }
                    return false;
                };
                $start.clone().prependTo($ul).attr("idx", help).click(_click).children(".close").click(_close);
                while (diff < 0) {
                    help++;
                    $start = $start.next();
                    start_w = $start.outerWidth();
                    _pos = $start.position().left + start_w; 
                    diff = _pos - absLft; 
                    $start.clone().prependTo($ul).attr("idx", help).click(_click).children(".close").click(_close);
                }
                _this.$moreWrap.css("left", offset.left).show();
            },
            click: function () {
                _this.$moreWrap.hide();
                var _$t = $(this);
                if (_$t.hasClass("k_tab_icon_btn_disabeld")) {
                    return;
                }
                var lft = _this.$ul.position().left;
                if (lft >= 0) {
                    _$t.addClass("k_tab_icon_btn_disabeld");
                    return;
                }
                var absLft = parseInt(Math.abs(lft));
                var $start = _this.$ul.children().first();
                var start_w = $start.outerWidth();
                var _pos = $start.position().left + start_w; 
                var diff = _pos - absLft; 
                while (diff < 0) {
                    $start = $start.next();
                    start_w = $start.outerWidth();
                    _pos = $start.position().left + start_w;
                    diff = _pos - absLft; 
                }
                var newLeft = lft + start_w - diff; 
                //var newLeft = diff;
                var shift = parseInt(newLeft);
                if (shift === _this.$ul.position().left) {
                    shift = parseInt(shift + $start.prev().outerWidth());
                }
                _this.$ul.animate({
                    "left": shift
                }, 200, function () {
                    if ($start.prev().length === 0) {
                        _$t.addClass("k_tab_icon_btn_disabeld");
                    }
                    _$t.trigger("mouseenter");
                });
                _this.rightButton.removeClass("k_tab_icon_btn_disabeld");
                return false;
            }
        });
        delete this.opts.tabs;             
        if (_this.opts.cxtmenu) {
            $("body").contextmenu(function () {
                return false;
            });
            var children = this.$ul.children().mousedown(function () {
                _this.ctxtarget = $(this);
            });
            if (typeof _this.opts.cxtmenu === 'array') {
                this.ctxmenu = new $B.Ctxmenu(children, _this.opts.cxtmenu);
            } else {
                this.ctxmenu = new $B.Ctxmenu(children, [{
                    text: $B.config.tabMenu.closeAll,
                    iconCls:'fa-cancel',
                    click: function () {
                        _this.$moreWrap.hide();
                        var rmItem = [];
                        var rmBody = [];
                        var liArray = _this.$ul.children();
                        liArray.each(function(i){
                            var li = $(this);
                            if(li.children("a.close").length > 0){
                                rmBody.push( _this.$body.children().eq(i) );
                                rmItem.push(li);
                            }
                        });
                        var go2Trigger = false;                        
                        for(var i = 0, len = rmItem.length ; i < len ; ++i){
                            if(rmItem[i].hasClass("actived")){
                                go2Trigger = true;
                            }
                            rmItem[i].remove();                            
                            rmBody[i].remove();
                        }
                        liArray = _this.$ul.children();
                        if(liArray.length > 0){
                            if(go2Trigger){
                                _this.$ul.animate({
                                    "left": 0
                                },200, function () {
                                    liArray.first().trigger("click");
                                });
                            }                            
                        }else{   
                            _this.$ul.removeData("activedItem");
                            _this.$ul.removeData("actived");
                        }
                    }
                }, {
                    text: $B.config.tabMenu.closeRight,
                    iconCls:'fa-forward-1',
                    click: function () {
                        _this.$moreWrap.hide();
                        _this.rightButton.addClass("k_tab_icon_btn_disabeld");
                        var rmItem = [];
                        var rmBody = [];
                        var isActivedLast = false;
                        _this.$ul.children().each(function (i) {
                            var $t = $(this);
                            if ($t.attr("title") === _this.ctxtarget.attr("title")) {
                                var $next = $t.next();
                                while ($next.length > 0) { 
                                    i++;                                  
                                    var isRm = $next.children("a.close").length > 0;
                                    if(isRm){
                                        if($next.hasClass("actived")){
                                            isActivedLast = true;
                                        }
                                        rmItem.push($next);
                                        rmBody.push(_this.$body.children().eq(i) );
                                    }
                                    $next = $next.next();                                                            
                                }
                                return false;
                            }
                        });
                        for(var i = 0, len = rmItem.length ; i < len ; ++i){                           
                            rmItem[i].remove();                            
                            rmBody[i].remove();
                        }
                        if(isActivedLast){
                            _this.$ul.children().last().trigger("click");                           
                        }
                        _this.$ul.animate({
                            "left": 0
                        },200, function () {                           
                        });
                    }
                }, {
                    text: $B.config.tabMenu.closeLeft,
                    iconCls:'fa-reply-1',
                    click: function () {  
                        _this.$moreWrap.hide();  
                        var rmItem = [];
                        var rmBody = [];
                        var isActivedFirst = false;                    
                        _this.$ul.children().each(function (i) {
                            var $t = $(this);
                            if ($t.attr("title") === _this.ctxtarget.attr("title")) {
                                return false;
                            }
                            var isRm = $t.children("a.close").length > 0;
                            if(isRm){
                                if($t.hasClass("actived")){
                                    isActivedFirst = true;
                                }
                                rmItem.push($t);
                                rmBody.push(_this.$body.children().eq(i) );
                            }                            
                        });
                        for(var i = 0, len = rmItem.length ; i < len ; ++i){                           
                            rmItem[i].remove();                            
                            rmBody[i].remove();
                        }
                        if(isActivedFirst){
                            _this.$ul.children().first().trigger("click");
                        }
                        _this.$ul.animate({
                            "left": 0
                        },200, function () {                           
                        });
                    }
                }, {
                    text: $B.config.tabMenu.reload,
                    iconCls:'fa-arrows-ccw',
                    click: function () {
                        _this.$moreWrap.hide();
                        _this.reload(_this.ctxtarget.attr("title"));
                    }
                }]);
            }
        }
    }
    Tab.prototype = {
        _scroll2show:function(li){
        },
        /***
         *激活一个tab 参数 title
         ****/
        active: function (title) {
            this.$moreWrap.hide();
            this.$ul.children().each(function (j) {
                var $t = $(this);
                if ($t.attr("title") === title) {
                    if (!$t.hasClass("actived")) {
                         $t.trigger("click");
                    }
                }
            });
        },
        /***
         *刷新一个tab 参数 title
         ****/
        reload: function (title) {
            this.$moreWrap.hide();
            var _this = this;
            var i = -1;
            this.$ul.children().each(function (j) {
                var $t = $(this);
                if ($t.attr("title") === title) {
                    i = j;
                    $t.trigger("click");
                    return false;
                }
            });
            if (i !== -1) {
                var $body = this.$body.children().eq(i);
                _load.call($body, _this.opts.onLoaded);
            }
        },
        /***
         *关闭一个tab 参数 title
         ****/
        close: function (title) {            
            var _this = this;
            var i = -1;
            this.$ul.children().each(function (j) {
                var $t = $(this);
                if ($t.attr("title") === title) {
                    i = j;
                    if ($t.hasClass("actived")) {
                        var next = $t.next();
                        if (next.length > 0) {
                            next.trigger("click");
                        } else {
                            var prev = $t.prev();
                            if (prev.length > 0) {
                                prev.trigger("click");
                            } else {
                                _this.removeData("activedItem");
                                _this.removeData("actived");
                            }
                        }
                    }
                    $t.remove();
                    return false;
                }
            });
            if (i !== -1) {
                this.$body.children().eq(i).remove();
            }
        },
        /**
         *新增一个tab 参数参考tab项参数
         ***/
        add: function (opt) {
            this.$moreWrap.hide();
            var liArray = this.$ul.children();
            //查询是否已经存在对应的tab
            var exist = false;
            liArray.each(function(){
                var $t = $(this);
                if(opt.title === $t.attr("title")){
                    exist = true;
                    $t.trigger("click");
                    return false;
                }
            });
            if(!exist){
                if(this.opts.tabCount){
                    if(liArray.length > this.opts.tabCount){
                        $B.alert($B.config.tabLimit.replace("[x]",this.opts.tabCount),3);
                        return;
                    }
                }
                opt["actived"] = true;
                var $li = _createItem.call(this, opt, this.$ul.height());
                var _this = this;
                $li.mousedown(function () {
                    _this.ctxtarget = $(this);
                });
                this.ctxmenu.bandTarget($li);
            }
        },
        /***
         * 获取当前激活的tab
         * ***/
        getActived:function(){
           return this.$ul.children("li.actived");
        }
    };
    $B["Tab"] = Tab;
    return Tab;
}));